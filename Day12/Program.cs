﻿var input = File.ReadAllLines("input.txt")
    .Select(line => (IList<char>)line.ToList())
    .ToList();
var grid = new Grid<char>(input);

var startCoord = grid.CoordOf('S');
grid[startCoord] = 'a';

var endCoord = grid.CoordOf('E');
grid[endCoord] = 'z';

ISet<Coord> GetNeighborsDown(Coord state)
{
    var current = grid[state];
    return new List<Coord> { (1, 0), (0, 1), (-1, 0), (0, -1) }
        .Select(dir => state + dir)
        .Where(coord =>
        {
            var next = grid[coord];
            return next != default && next >= current - 1;
        })
        .ToHashSet();
}

var a1 = ShortestPathBfs(endCoord, it => it == startCoord, GetNeighborsDown);
Console.WriteLine($"Answer 1: {a1}");

var a2 = ShortestPathBfs(endCoord, it => grid[it] == 'a', GetNeighborsDown);
Console.WriteLine($"Answer 2: {a2}");

int ShortestPathBfs<T>(T source, Predicate<T> isTarget, Func<T, ISet<T>> getNeighbors)
{
    var queue = new Queue<(T, int)>(new List<(T, int)> { (source, 0) });
    var visited = new HashSet<T>();

    while (queue.TryDequeue(out var next))
    {
        var (state, cost) = next;
        if (isTarget(state)) return cost;
        if (!visited.Add(state)) continue;
        foreach (var nextState in getNeighbors(state))
        {
            queue.Enqueue((nextState, cost + 1));
        }
    }

    throw new ApplicationException("No path to target");
}

file record Coord
{
    public int X { private init; get; }
    public int Y { get; private init; }
    public static implicit operator Coord((int, int) tuple) => new Coord { X = tuple.Item1, Y = tuple.Item2 };
    public static Coord operator +(Coord a, Coord b) => (a.X + b.X, a.Y + b.Y);
}

file class Grid<T>
{
    private readonly List<IList<T>> _grid;

    public Grid(List<IList<T>> grid)
    {
        _grid = grid;
    }

    public T? this[Coord c]
    {
        get
        {
            try
            {
                return _grid[c.Y][c.X];
            }
            catch (ArgumentOutOfRangeException)
            {
                return default;
            }
        }
        set => _grid[c.Y][c.X] = value ?? throw new ArgumentNullException(nameof(value));
    }

    public Coord CoordOf(T value)
    {
        var y = _grid.FindIndex(line => line.Contains(value));
        if (y == -1) throw new ArgumentException("Grid does not contain value");
        var x = _grid[y].IndexOf(value);
        return (x, y);
    }
}