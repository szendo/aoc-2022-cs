﻿var input = File.ReadAllLines("input.txt");

var sizes = new Dictionary<string, long>();
var lsInDir = new Dictionary<string, long>();

var currentPath = string.Empty;

foreach (var line in input)
{
    if (line.StartsWith("$ "))
    {
        var command = line[2..];
        switch (command)
        {
            case "ls":
                lsInDir[currentPath] = lsInDir.GetValueOrDefault(currentPath) + 1;
                break;

            case "cd /":
                currentPath = string.Empty;
                break;

            case "cd ..":
                currentPath = currentPath[..currentPath.LastIndexOf("/", StringComparison.Ordinal)];
                break;

            default: // cd xyz
                currentPath = currentPath + "/" + command[3..];
                break;
        }
    }
    else
    {
        if (lsInDir[currentPath] == 1)
        {
            var sizeOrDir = line.Split(" ")[0];
            if (sizeOrDir != "dir")
            {
                sizes[currentPath] = sizes.GetValueOrDefault(currentPath) + long.Parse(sizeOrDir);
            }
        }
    }
}

var totalSizes = new Dictionary<string, long>();
foreach (var dir in sizes.Keys)
{
    var cdir = dir;
    do
    {
        if (totalSizes.ContainsKey(cdir))
        {
            cdir = cdir[..cdir.LastIndexOf("/", StringComparison.Ordinal)];
            continue;
        }

        var totalSize = sizes.Keys.Where(k => k.StartsWith(cdir)).Sum(k => sizes[k]);
        totalSizes[cdir] = totalSize;

        if (cdir != string.Empty)
            cdir = cdir[..cdir.LastIndexOf("/", StringComparison.Ordinal)];
    } while (cdir != string.Empty);
}

Console.WriteLine($"Answer 1: {totalSizes.Values.Where(a => a <= 100000).Sum()}");

var free = 70000000 - totalSizes[""];
var delSize = totalSizes.Values
    .OrderBy(size => size)
    .First(size => free + size > 30000000);
Console.WriteLine($"Answer 2: {delSize}");