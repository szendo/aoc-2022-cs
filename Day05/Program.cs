﻿// [P]     [C]         [M]            
// [D]     [P] [B]     [V] [S]        
// [Q] [V] [R] [V]     [G] [B]        
// [R] [W] [G] [J]     [T] [M]     [V]
// [V] [Q] [Q] [F] [C] [N] [V]     [W]
// [B] [Z] [Z] [H] [L] [P] [L] [J] [N]
// [H] [D] [L] [D] [W] [R] [R] [P] [C]
// [F] [L] [H] [R] [Z] [J] [J] [D] [D]
//  1   2   3   4   5   6   7   8   9 

var inputStacks = new List<List<char>>
{
    "FHBVRQDP".ToList(),
    "LDZQWV".ToList(),
    "HLZQGRPC".ToList(),
    "RDHFJVB".ToList(),
    "ZWLC".ToList(),
    "JRPNTGVM".ToList(),
    "JRLVMBS".ToList(),
    "DPJ".ToList(),
    "DCNWV".ToList(),
};

List<List<char>> MoveCrates(List<List<char>> stacks, List<string> moves, bool reverseOrder = true)
{
    var result = new List<List<char>>(stacks);
    foreach (var line in moves)
    {
        var nums = line.Split(' ').Select(int.Parse).ToList();
        var count = nums[0];
        var from = nums[1] - 1;
        var to = nums[2] - 1;

        var cratesToMove = result[from].TakeLast(count);
        if (reverseOrder)
        {
            cratesToMove = cratesToMove.Reverse();
        }

        result[from] = result[from].SkipLast(count).ToList();
        result[to] = result[to].Concat(cratesToMove).ToList();
    }

    return result;
}

string GetTopCrates(List<List<char>> stacks) => string.Concat(stacks.Select(it => it.Last()));

// count from to
var moves = File.ReadAllLines("moves.txt").ToList();

Console.WriteLine($"Answer 1: {GetTopCrates(MoveCrates(inputStacks, moves))}");
Console.WriteLine($"Answer 2: {GetTopCrates(MoveCrates(inputStacks, moves, reverseOrder: false))}");
