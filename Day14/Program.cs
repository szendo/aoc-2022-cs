﻿int SimulateSand(ICollection<Coord> rocks, bool infiniteFloor)
{
    var fallDirections = new List<Coord> { (0, 1), (-1, 1), (1, 1) };
    var maxDepth = rocks.Max(c => c.Y);
    var sands = new HashSet<Coord>();

    while (true)
    {
        Coord sand = (500, 0);

        while (true)
        {
            if (!infiniteFloor && sand.Y > maxDepth) break;

            var blocked = true;
            foreach (var direction in fallDirections)
            {
                var sandNext = sand + direction;
                if (rocks.Contains(sandNext)
                    || sands.Contains(sandNext)
                    || (infiniteFloor && sand.Y > maxDepth)) continue;

                sand = sandNext;
                blocked = false;
                break;
            }

            if (blocked)
            {
                sands.Add(sand);
                break;
            }
        }

        if (sand == (500, 0) || !infiniteFloor && sand.Y > maxDepth) break;
    }

    return sands.Count;
}

var input = File.ReadAllLines("input.txt")
    .Select(line =>
    {
        return line.Split(" -> ")
            .Select(s =>
            {
                var coords = s.Split(',', 2);
                return new Coord(int.Parse(coords[0]), int.Parse(coords[1]));
            })
            .ToList();
    })
    .ToList();

var rocks = new HashSet<Coord>();
foreach (var path in input)
{
    var lastPoint = path[0];
    foreach (var point in path)
    {
        if (point.X == lastPoint.X)
        {
            var minY = Math.Min(point.Y, lastPoint.Y);
            var maxY = Math.Max(point.Y, lastPoint.Y);
            for (var y = minY; y <= maxY; y++)
            {
                rocks.Add((point.X, y));
            }
        }
        else if (point.Y == lastPoint.Y)
        {
            var minX = Math.Min(point.X, lastPoint.X);
            var maxX = Math.Max(point.X, lastPoint.X);
            for (var x = minX; x <= maxX; x++)
            {
                rocks.Add((x, point.Y));
            }
        }
        else
        {
            throw new ArgumentException("Line is not horizontal or vertical");
        }

        lastPoint = point;
    }
}

Console.WriteLine($"Answer 1: {SimulateSand(rocks, infiniteFloor: false)}");
Console.WriteLine($"Answer 1: {SimulateSand(rocks, infiniteFloor: true)}");

file record Coord(int X, int Y)
{
    public int X { get; } = X;
    public int Y { get; } = Y;
    public static implicit operator Coord((int, int) tuple) => new(tuple.Item1, tuple.Item2);
    public static Coord operator +(Coord a, Coord b) => (a.X + b.X, a.Y + b.Y);
}