﻿bool FullyContains(Range r1, Range r2) => r1.Start.Value <= r2.Start.Value && r1.End.Value >= r2.End.Value;

bool Overlaps(Range r1, Range r2) => !(
    r1.End.Value  < r2.Start.Value || r1.Start.Value > r2.End.Value 
);

var input = File.ReadAllLines("input.txt");

var rangePairs = input.Select(line => line.Split(",", 2)
        .Select(it =>
        {
            var range = it.Split("-", 2);
            return int.Parse(range[0])..int.Parse(range[1]);
        }).ToList()
    )
    .Select(a => (a[0], a[1]))
    .ToList();

var contains = rangePairs.Count((a) => FullyContains(a.Item1, a.Item2) || FullyContains(a.Item2, a.Item1));
Console.WriteLine($"Answer 1: {contains}");

var overlaps = rangePairs.Count((a) => Overlaps(a.Item2, a.Item1));
Console.WriteLine($"Answer 2: {overlaps}");