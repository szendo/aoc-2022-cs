﻿var input = File.ReadAllLines("input.txt");

var allSums = new List<int>();
var currentSum = 0;

foreach (var s in input)
{
    if (string.IsNullOrEmpty(s))
    {
        allSums.Add(currentSum);
        currentSum = 0;
        continue;
    }

    currentSum += int.Parse(s);
}

allSums.Add(currentSum);
allSums.Sort();

Console.WriteLine($"Answer 1: {allSums.Last()}");
Console.WriteLine($"Answer 2: {allSums.TakeLast(3).Sum()}");