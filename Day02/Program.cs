﻿int GetScore(char opponent, char own)
{
    var oppNum = opponent - 'A';
    var ownNum = own - 'X';
    var shapeScore = ownNum + 1;
    var outcomeScore = oppNum == ownNum ? 3 : (oppNum + 1) % 3 == ownNum ? 6 : 0;
    return shapeScore + outcomeScore;
}

char GetOwnShape(char opponent, char outcome)
{
    var oppNum = opponent - 'A';
    var outcomeNum = outcome - 'X' - 1;
    var ownNum = (oppNum + 3 + outcomeNum) % 3;
    return Convert.ToChar('X' + ownNum);
}

var input = File.ReadAllLines("input.txt");

Console.WriteLine($"Answer 1: {input.Sum(l => GetScore(l[0], l[2]))}");
Console.WriteLine($"Answer 2: {input.Sum(l => GetScore(l[0], GetOwnShape(l[0], l[2])))}");