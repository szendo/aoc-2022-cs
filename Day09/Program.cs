﻿var input = File.ReadAllLines("input.txt");

var rope = Enumerable.Repeat(Coord.Origin, 10).ToList();
var visited1 = new HashSet<Coord> { Coord.Origin };
var visited2 = new HashSet<Coord> { Coord.Origin };

foreach (var command in input)
{
    var commandParts = command.Split(' ', 2);
    var dir = commandParts[0][0];
    var len = int.Parse(commandParts[1]);

    Coord delta = dir switch
    {
        'U' => (0, 1),
        'D' => (0, -1),
        'L' => (-1, 0),
        'R' => (1, 0),
        _ => throw new ArgumentException($"Invalid direction: {dir}")
    };

    for (var i = 0; i < len; i++)
    {
        rope[0] += delta;

        for (var j = 1; j < rope.Count; j++)
        {
            if (rope[j].Distance(rope[j - 1]) > 1)
            {
                rope[j] += (rope[j - 1] - rope[j]).Sign();
            }
        }

        visited1.Add(rope[1]);
        visited2.Add(rope.Last());
    }
}

Console.WriteLine($"Answer 1: {visited1.Count}");
Console.WriteLine($"Answer 2: {visited2.Count}");

file record Coord
{
    public static readonly Coord Origin = (0, 0);

    public static implicit operator Coord((int, int) tuple) => new Coord { _x = tuple.Item1, _y = tuple.Item2 };

    private int _x;
    private int _y;

    public static Coord operator +(Coord a, Coord b) => (a._x + b._x, a._y + b._y);
    public static Coord operator -(Coord a, Coord b) => (a._x - b._x, a._y - b._y);
    public int Distance(Coord b) => Math.Max(Math.Abs(_x - b._x), Math.Abs(_y - b._y));

    public Coord Sign() => (Math.Sign(_x), Math.Sign(_y));
}