﻿var input = File.ReadLines("input.txt")
    .Select(line =>
    {
        var parts = line.Split('=', ',', ':');
        var sx = int.Parse(parts[1]);
        var sy = int.Parse(parts[3]);
        var bx = int.Parse(parts[5]);
        var by = int.Parse(parts[7]);
        Coord sensor = (sx, sy);
        Coord beacon = (bx, by);
        return (sensor, beacon);
    })
    .ToList();

// const int refY = 10;
const int refY = 2000000;

var excluded = new HashSet<int>();
foreach (var (sensor, beacon) in input)
{
    var d = sensor.Distance(beacon);
    var rd = sensor.Distance((sensor.X, refY));
    if (rd >= d) continue;

    var minX = sensor.X - (d - rd);
    var maxX = sensor.X + (d - rd);
    for (var x = minX; x <= maxX; x++)
    {
        if (beacon != (x, refY))
        {
            excluded.Add(x);
        }
    }
}

Console.WriteLine($"Answer 1: {excluded.Count}");

bool IsOutOfReach(IEnumerable<(Coord, Coord)> sensorsWithClosestBeacon, Coord coord)
{
    foreach (var (sensor, beacon) in sensorsWithClosestBeacon)
    {
        if (sensor.Distance(beacon) >= sensor.Distance(coord)) return false;
    }

    return true;
}

Coord FindDistressBeacon(IList<(Coord, Coord)> sensorsWithClosestBeacon)
{
    foreach (var (sensor, beacon) in sensorsWithClosestBeacon)
    {
        var d = sensor.Distance(beacon) + 1;
        for (var y = 0; y <= 2 * refY; y++)
        {
            var rd = sensor.Distance((sensor.X, y));
            if (rd > d) continue;

            foreach (var bx in new List<int> { sensor.X - (d - rd), sensor.X + (d - rd) })
            {
                if (bx is < 0 or > 2 * refY) continue;
                Coord b = (bx, y);
                if (IsOutOfReach(sensorsWithClosestBeacon, b))
                {
                    return b;
                }
            }
        }
    }

    throw new ApplicationException("Distress beacon not found");
}

long GetTuningFrequency(Coord coord) => 4000000L * coord.X + coord.Y;

Console.WriteLine($"Answer 2: {GetTuningFrequency(FindDistressBeacon(input))}");

file record Coord(int X, int Y)
{
    public int X { get; } = X;
    public int Y { get; } = Y;
    public static implicit operator Coord((int, int) tuple) => new(tuple.Item1, tuple.Item2);
    public static Coord operator +(Coord a, Coord b) => (a.X + b.X, a.Y + b.Y);
    public int Distance(Coord other) => Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
}