﻿var input = File.ReadAllLines("input.txt");

var sum1 = input
    .Select(line =>
    {
        var len = line.Length;
        var c1 = line[..(len / 2)].ToHashSet();
        var c2 = line[(len / 2)..].ToHashSet();
        return c1.Intersect(c2).First();
    })
    .Sum(c => char.IsLower(c) ? c - 'a' + 1 : c - 'A' + 27);

Console.WriteLine($"Answer 1: {sum1}");

var sum2 = input.Chunk(3)
    .Select(chunk => chunk[0].ToHashSet().Intersect(chunk[1]).Intersect(chunk[2]).First())
    .Sum(c => char.IsLower(c) ? c - 'a' + 1 : c - 'A' + 27);

Console.WriteLine($"Answer 2: {sum2}");