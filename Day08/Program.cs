﻿var trees = File.ReadAllLines("input.txt")
    .Select(row => row.Select(c => int.Parse(c.ToString())).ToList())
    .ToList();

var size = trees.Count;
var lastIndex = size - 1;

var top = Enumerable.Repeat(-1, size).ToList();
var bottom = Enumerable.Repeat(-1, size).ToList();
var left = Enumerable.Repeat(-1, size).ToList();
var right = Enumerable.Repeat(-1, size).ToList();

var visible = new HashSet<(int, int)>();

void CheckTreesFromEdge(int x, int y, bool isXAcross, IList<int> edge)
{
    var across = isXAcross ? x : y;
    if (trees[y][x] <= edge[across]) return;
    visible.Add((y, x));
    edge[across] = trees[y][x];
}

for (var inward = 0; inward < size; inward++)
{
    for (var across = 0; across < size; across++)
    {
        CheckTreesFromEdge(across, inward, true, top);
        CheckTreesFromEdge(across, lastIndex - inward, true, bottom);
        CheckTreesFromEdge(inward, across, false, left);
        CheckTreesFromEdge(lastIndex - inward, across, false, right);
    }
}

Console.WriteLine($"Answer 1: {visible.Count}");

var maxScore = 0;
for (var y = 1; y < lastIndex; y++)
{
    for (var x = 1; x < lastIndex; x++)
    {
        var height = trees[y][x];
        var score = new List<(int, int)> { (1, 0), (-1, 0), (0, 1), (0, -1) }
            .Select(d =>
            {
                var (dx, dy) = d;
                var k = 1;
                while (dx > 0 ? x + k < lastIndex
                       : dx < 0 ? x - k > 0
                       : dy > 0 ? y + k < lastIndex
                       : y - k > 0)
                {
                    if (trees[y + dy * k][x + dx * k] >= height) break;
                    k++;
                }

                return k;
            })
            .Aggregate((a, b) => a * b);

        maxScore = Math.Max(score, maxScore);
    }
}

Console.WriteLine($"Answer 2: {maxScore}");
