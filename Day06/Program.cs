﻿int AllDistinctAt(string s, int length)
{
    for (var index = 0; index < s.Length - length; index++)
        if (s.Substring(index, length).ToHashSet().Count == length)
            return index + length;

    return -1;
}

var input = File.ReadAllLines("input.txt")[0];

Console.WriteLine($"Answer 1: {AllDistinctAt(input, 4)}");
Console.WriteLine($"Answer 2: {AllDistinctAt(input, 14)}");