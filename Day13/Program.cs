﻿var input = File.ReadAllLines("input.txt")
    .Where(it => it != string.Empty)
    .Select(Data.Parse)
    .ToList();

var pairs = input
    .Chunk(2)
    .Select(it => (it[0], it[1]))
    .ToList();

var a1 = 0;
for (var i = 0; i < pairs.Count; i++)
{
    var (left, right) = pairs[i];
    if (left.CompareTo(right) < 1) a1 += i + 1;
}

Console.WriteLine($"Answer 1: {a1}");

var allPackets = new List<Data>(input);
var divider1 = Data.Parse("[[2]]");
allPackets.Add(divider1);
var divider2 = Data.Parse("[[6]]");
allPackets.Add(divider2);
allPackets.Sort();
Console.WriteLine($"Answer 2: {(allPackets.IndexOf(divider1) + 1) * (allPackets.IndexOf(divider2) + 1)}");

file abstract record Data : IComparable<Data>
{
    public abstract List<Data> GetDataAsList();

    public int CompareTo(Data? other)
    {
        if (other == null) throw new ArgumentNullException(nameof(other));

        if (this is IntData thisInt && other is IntData otherInt)
        {
            return thisInt.Value.CompareTo(otherInt.Value);
        }

        var thisList = GetDataAsList();
        var otherList = other.GetDataAsList();

        var count = Math.Min(thisList.Count, otherList.Count);
        for (var i = 0; i < count; i++)
        {
            var compareResult = thisList[i].CompareTo(otherList[i]);
            if (compareResult != 0) return compareResult;
        }

        return thisList.Count - otherList.Count;
    }

    public static Data Parse(string value)
    {
        var index = 0;
        return Parse(value, ref index);
    }

    private static Data Parse(string value, ref int index)
    {
        if (value[index] == '[')
        {
            index++;

            var list = new List<Data>();

            while (true)
            {
                switch (value[index])
                {
                    case ']':
                        index++;
                        return new ListData(list);
                    case ',':
                        index++;
                        break;
                    default:
                        list.Add(Parse(value, ref index));
                        break;
                }
            }
        }

        var part = value[index..].Split(new[] { ',', '[', ']' }, 2)[0];
        index += part.Length;
        return new IntData(int.Parse(part));
    }
}

file sealed record IntData(int Value) : Data
{
    public override List<Data> GetDataAsList() => new() { this };
    public override string ToString() => Value.ToString();
}

file sealed record ListData(List<Data> Value) : Data
{
    public override List<Data> GetDataAsList() => Value;
    public override string ToString() => $"[{string.Join(',', Value.Select(it => it.ToString()))}]";
}